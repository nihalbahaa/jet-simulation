
# coding: utf-8

# In[3]:

import threading
from threading import Thread

import math
import random
import numpy as np
import math as m
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D




# FUNCTIONS
def Theta():                      #define a function to generate random theta 
    ytest = np.log(1+1.5707963267948966)*random.random() #choose theta between 0 and 90
    # because exp is the inverse of log(x)
    # and log(x) is the CDF of the PDF 1/x
    xtest = np.exp(ytest)-1 
    return xtest 
def Phi():                      #define a function to generate random theta 
    ytest = np.log(1+6.283185307179586)*random.random() #choose phi between 0 and 360
    # because exp is the inverse of log(x)
    # and log(x) is the CDF of the PDF 1/x
    xtest = np.exp(ytest)-1
    return xtest
def Zvalue(): #x-a/b-a=[0,1] where a min of the interval ,and b the max which we want generate value between them
    ytest=(random.random()*(1.0982000000000003))+3.2488  #choose random number between log(25)and log(75)
    # because exp is the inverse of log(x)
    # and log(x) is the CDF of the PDF 1/x
    xtest = np.exp(ytest) 
    return int(xtest)   
def initail_line():
    ax.plot((0,pInitial[0]),(0,pInitial[1]),(0,pInitial[2]))
    return 0
    
def glune_diffusion(pInitialStart,pRadStart,Erad,Ecr):

    Ef2=Erad/2
    Theta1=Theta()
    Phi1=Phi()
    
    mInitial = [pRadStart[0]-pInitialStart[0] , pRadStart[1]-pInitialStart[1] , pRadStart[2]-pInitialStart[2]]
    
    mRad      = [0,0,0]
    mRad[0]   = Ef2*m.cos(Theta1)*m.cos(Phi1)
    mRad[1]   = Ef2*m.cos(Theta1)*m.sin(Phi1)
    mRad[2]   = Ef2*m.sin(Theta1)
    
    mFinal    = [0,0,0]
    mFinal[0] = mInitial[0]-mRad[0]
    mFinal[1] = mInitial[1]-mRad[1]
    mFinal[2] = mInitial[2]-mRad[2]
    
    pRad   = [pRadStart[0] + mRad[0],   pRadStart[1] + mRad[1],   pRadStart[2] + mRad[2]]  #calculate the next poin using theta/2
    pFinal = [pRadStart[0] + mFinal[0], pRadStart[1] + mFinal[1], pRadStart[2] + mFinal[2]]    #calculate the point in the opposite direction 

    mRad   = [pRad[0]-pRadStart[0] , pRad[1]-pRadStart[1] , pRad[2]-pRadStart[2]]
    mFinal = [pFinal[0]-pRadStart[0] , pFinal[1]-pRadStart[1] , pFinal[2]-pRadStart[2]]
    
    EnergyMag_initial = (mInitial[0]**2+mInitial[1]**2+mInitial[2]**2)**0.5
    EnergyMag_final   = ((mFinal[0]+mRad[0])**2+(mFinal[1]+mRad[1])**2+(mFinal[2]+mRad[2])**2)**0.5
    
    print("==================================")
    print("Initial Gluon Check :")
    print("Initial : ",mInitial[0],mInitial[1],mInitial[2], EnergyMag_initial )
    print("Rad1    : ",mRad[0],mRad[1],mRad[2])
    print("Rad2    : ",mFinal[0],mFinal[1],mFinal[2])
    print("SumRad  : ",mFinal[0]+mRad[0],mFinal[1]+mRad[1],mFinal[2]+mRad[2], EnergyMag_final)
    print("==================================")

    ax.plot([pRadStart[0],pRad[0]],  [pRadStart[1],pRad[1]],  [pRadStart[2],pRad[2]])
    ax.plot([pRadStart[0],pFinal[0]],[pRadStart[1],pFinal[1]],[pRadStart[2],pFinal[2]])
    
    
    
    
#     p3 =[p1[0]+ Ef*m.sin(Theta1)*m.cos(Phi1),p1[1]+ Ef*m.cos(Theta1)*m.sin(Phi1),p1[2]+Ef*m.cos(Theta1)]  #calculate the next poin using theta/2
#     p4 =[p1[0]+Ef*m.sin(Theta2)*m.cos(Phi2),p1[1]-Ef*m.cos(Theta2)*m.sin(Phi2),p1[2]+ Ef*m.cos(Theta2)]    #calculate the point in the opposite direction 
#     ax.plot([p1[0],p3[0]],[p1[2],p3[2]],[p1[1],p3[1]])
#     ax.plot([p1[0],p4[0]],[p1[2],p4[2]],[p1[1],p4[1]])
#     
#     m3 = [p3[0]-p1[0] , p3[1]-p1[1] , p3[2]-p1[2]]
#     m4 = [p4[0]-p1[0] , p4[1]-p1[1] , p4[2]-p1[2]]
#     
#     print("==================================")
#     print("Gluon diffusion check :")
#     print("Initial : ",m1[0],m1[1],m1[2])
#     print("Rad1    : ",m3[0],m3[1],m3[2])
#     print("Rad2    : ",m4[0],m4[1],m4[2])
#     print("SumRad  : ",m4[0]+m3[0],m4[1]+m3[1],m4[2]+m3[2])
#     print("==================================")
#     
    if Ef2<=Ecr:
        a.append([mFinal[0]+mRad[0],mFinal[1]+mRad[1],mFinal[2]+mRad[2]])
        b.append(2*Ef2)
        c.append(Ef2)
        c.append(Ef2)         
    else:
        quark_diffusion(pRadStart, pRad,  Ef2,Ecr)
        quark_diffusion(pRadStart, pFinal,Ef2,Ecr)
    print("-----------Rad energy",Ef2,"Final magEnergy = ",(mRad[0]**2+mRad[1]**2+mRad[2]**2)**0.5)
    print("-----------final energy",Ef2,"Final magEnergy = ",(mFinal[0]**2+mFinal[1]**2+mFinal[2]**2)**0.5)

def quark_diffusion(pInitialStart, pFinalStart, Ef, Ecr):                  #define a function to make branches 
    
    Erad1 = Ef*(Zvalue()/100)                             #calculate the energy radition using inetial energy and z
    Ef1   = Ef-Erad1 

    Theta1=Theta()
    Phi1=Phi()

    mInitial = [pFinalStart[0]-pInitialStart[0] , pFinalStart[1]-pInitialStart[1] , pFinalStart[2]-pInitialStart[2]]
    
    mRad      = [0,0,0]
    mRad[0]   = Erad1*m.cos(Theta1)*m.cos(Phi1)
    mRad[1]   = Erad1*m.cos(Theta1)*m.sin(Phi1)
    mRad[2]   = Erad1*m.sin(Theta1)
    
    mFinal    = [0,0,0]
    mFinal[0] = mInitial[0]-mRad[0]
    mFinal[1] = mInitial[1]-mRad[1]
    mFinal[2] = mInitial[2]-mRad[2]
    
    pRad   = [pFinalStart[0] + mRad[0],   pFinalStart[1] + mRad[1],   pFinalStart[2] + mRad[2]]  #calculate the next poin using theta/2
    pFinal = [pFinalStart[0] + mFinal[0], pFinalStart[1] + mFinal[1], pFinalStart[2] + mFinal[2]]    #calculate the point in the opposite direction 
    
    mRad   = [pRad[0]-pFinalStart[0] , pRad[1]-pFinalStart[1] , pRad[2]-pFinalStart[2] ]
    mFinal = [pFinal[0]-pFinalStart[0], pFinal[1]-pFinalStart[1] , pFinal[2]-pFinalStart[2]]

        
    EnergyMag_initial = (mInitial[0]**2+mInitial[1]**2+mInitial[2]**2)**0.5
    EnergyMag_final   = ((mFinal[0]+mRad[0])**2+(mFinal[1]+mRad[1])**2+(mFinal[2]+mRad[2])**2)**0.5
    
   # print("==================================")
   # print("Initial Quark Check :")
   # print("Initial : ",mInitial[0],mInitial[1],mInitial[2], EnergyMag_initial )
   # print("Rad1    : ",mRad[0],mRad[1],mRad[2])
   # print("Rad2    : ",mFinal[0],mFinal[1],mFinal[2])
   # print("SumRad  : ",mFinal[0]+mRad[0],mFinal[1]+mRad[1],mFinal[2]+mRad[2], EnergyMag_final)
   # print("==================================")

    ax.plot([pFinalStart[0],pRad[0]],  [pFinalStart[1],pRad[1]],  [pFinalStart[2],pRad[2]])
    ax.plot([pFinalStart[0],pFinal[0]],[pFinalStart[1],pFinal[1]],[pFinalStart[2],pFinal[2]])
    
    if(Ef1>=Ecr and Erad1>=Ecr):
        Thread(target = glune_diffusion(pFinalStart,pFinal,Erad1,Ecr)).start()
        Thread(target = quark_diffusion(pFinalStart,pRad,Ef1,Ecr)).start()
        
    elif Ef1>=Ecr:
        quark_diffusion(pFinalStart,pFinal,Ef1,Ecr)
        a.append([mRad[0],mRad[1],mRad[2]])
        b. append(Erad1)
        c.append(Erad1)
    elif Erad1>=Ecr:
        glune_diffusion(pFinalStart,pRad,Erad1,Ecr)
        a.append([mFinal[0],mFinal[1],mFinal[2]])
        b.append(Ef1)
        c.append(Ef1)
    else:
        a.append([mFinal[0]+mRad[0],mFinal[1]+mRad[1],mFinal[2]+mRad[2]])
        b.append(Ef1+Erad1)
        c.append(Erad1)
        c.append(Ef1)         

   # print("***********-Rad energy",Erad1,"Final magEnergy = ",(mRad[0]**2+mRad[1]**2+mRad[2]**2)**0.5)
   # print("***********-final energy",Ef1,"Final magEnergy = ",(mFinal[0]**2+mFinal[1]**2+mFinal[2]**2)**0.5)            
if __name__ == '__main__':
    a=[]
    b=[]
    c=[]
    PseudoJets=0
    energy=0
    momentum1=0
    momentum2=0
    momentum3=0
    Ecr = 15    
    Ei  = 100
    
   # print("Energy :")
   # print("Initial  : ",Ei)
   # print("Critical : ",Ecr)
    
    DirectionInitial=[1,1,2]
    
    DirectionInitialMag = (DirectionInitial[0]**2 + DirectionInitial[1]**2 + DirectionInitial[2]**2)**0.5
    
    DirectionInitial[0] = DirectionInitial[0]*(1.0/DirectionInitialMag)
    DirectionInitial[1] = DirectionInitial[1]*(1.0/DirectionInitialMag)
    DirectionInitial[2] = DirectionInitial[2]*(1.0/DirectionInitialMag)
    
   # print("Direction : ",DirectionInitial)
    
    pInitial    = [0,0,0]
    pInitial[0] = Ei * DirectionInitial[0]
    pInitial[1] = Ei * DirectionInitial[1]
    pInitial[2] = Ei * DirectionInitial[2]
    
   # print("FirstPoint : ",pInitial[0],pInitial[1],pInitial[2])
    
    EnergyValidation = (pInitial[0]**2 + pInitial[1]**2 + pInitial[2]**2)**0.5
    
   # print("EnergyValidation : ",EnergyValidation)
    
    Theta1=Theta()
    Phi1=Phi()
    

    
    Erad=Ei*(Zvalue() /100)                             #calculate the energy radition using inetial energy and z
    Ef=Ei-Erad  
   
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    
    mInitial  = pInitial # -[0,0,0]
    
    mRad      = [0,0,0]
    mRad[0]   = Erad*m.cos(Theta1)*m.cos(Phi1)
    mRad[1]   = Erad*m.cos(Theta1)*m.sin(Phi1)
    mRad[2]   = Erad*m.sin(Theta1)
  
    mFinal    = [0,0,0]
    mFinal[0] = mInitial[0]-mRad[0]
    mFinal[1] = mInitial[1]-mRad[1]
    mFinal[2] = mInitial[2]-mRad[2]
  
    pRad   = [pInitial[0] + mRad[0],   pInitial[1] + mRad[1],   pInitial[2] + mRad[2]]  #calculate the next poin using theta/2
    pFinal = [pInitial[0] + mFinal[0], pInitial[1] + mFinal[1], pInitial[2] + mFinal[2]]    #calculate the point in the opposite direction 

    mRad   = [pRad[0]-pInitial[0] , pRad[1]-pInitial[1] , pRad[2]-pInitial[2]]
    mFinal = [pFinal[0]-pInitial[0] , pFinal[1]-pInitial[1] , pFinal[2]-pInitial[2]]
   
    EnergyMag_initial = (mInitial[0]**2+mInitial[1]**2+mInitial[2]**2)**0.5
    EnergyMag_final   = ((mFinal[0]+mRad[0])**2+(mFinal[1]+mRad[1])**2+(mFinal[2]+mRad[2])**2)**0.5
 
   # print("==================================")
   # print("Initial Splitting Check :")
   # print("Initial : ",mInitial[0],mInitial[1],mInitial[2], EnergyMag_initial )
   # print("Rad1    : ",mRad[0],mRad[1],mRad[2])
   # print("Rad2    : ",mFinal[0],mFinal[1],mFinal[2])
   # print("SumRad  : ",mFinal[0]+mRad[0],mFinal[1]+mRad[1],mFinal[2]+mRad[2], EnergyMag_final)
   # print("==================================")

    ax.plot([pInitial[0],pRad[0]],[pInitial[1],pRad[1]],[pInitial[2],pRad[2]])
    ax.plot([pInitial[0],pFinal[0]],[pInitial[1],pFinal[1]],[pInitial[2],pFinal[2]])
    
   # print("-----------Rad energy",Erad,"Final magEnergy = ",(mRad[0]**2+mRad[1]**2+mRad[2]**2)**0.5)
   # print("-----------final energy",Ef,"Final magEnergy = ",(mFinal[0]**2+mFinal[1]**2+mFinal[2]**2)**0.5)
   
    Thread(target = initail_line()).start()
    Thread(target = glune_diffusion(pInitial,pRad,  Erad,Ecr)).start()
    Thread(target = quark_diffusion(pInitial,pFinal,Ef,  Ecr)).start()
    for i in range (len(b)):
        energy=energy+b[i]
    if Ei ==energy:
       # print("initial energy =",Ei,"final energy =",energy)
       # print("===========================================================================")
       # print("---------------------------the Energy is coserved--------------------------") 
       # print("===========================================================================")

   # print("energy of final state (PseudoJets)" ,c,"    the number of PseudoJets",len(c))    
   # print("===========================================================================")       
    for i in range(len(a)):
        momentum1=momentum1+a[i][0]
    x=momentum1    
    for i in range(len(a)):
        momentum2=momentum2+a[i][1] 
    y=momentum2    
    for i in range(len(a)):
        momentum3=momentum3+a[i][2]
    z=momentum3
   # print("initial momentum vector = ",pInitial,"final momentum vector = ",x,y,z)
   # print(" EnergyMag_initial ", EnergyMag_initial," EnergyMag_final ",(x**2+y**2+z**2)**0.5)
   # print("================================================================================")
   # print("----------------------------momentum is conserved-------------------------------")
   # print("================================================================================")
   # print(a)
    plt.show()

