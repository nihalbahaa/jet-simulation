
# coding: utf-8

# In[6]:

import matplotlib.pyplot as plt
import numpy as np
import random
import pylab 


pdf_xvals=np.arange(0.25,5,0.1)
pdf_yvals=[]

cdf_xvals=np.arange(0.25,5,0.1)
cdf_yvals=[]

line=[]

for x in pdf_xvals:

    pdf = 1.0/x
    pdf_yvals.append(pdf)

    cdf = np.log(x)
    cdf_yvals.append(cdf)

    line.append(0.0)
plt.ylim((-1,1.52))
plt.plot(pdf_xvals,pdf_yvals,"b-",label='pdf=1/x')

plt.plot(cdf_xvals,cdf_yvals,"r-",label='cdf=log')
pylab.legend(loc='upper right')
plt.plot(cdf_xvals,line,"black")
plt.ylabel('mc testing')
plt.show()


n=10000

meas_accrej=[]

i=0

totTests   = 0
totAccepts = 0

while i<n:

    repeatFlag = 1
    while repeatFlag:
        xtest = (2.0 * random.random()) + 1.0  # [1,3]
        ytest = random.random() 
        func  = 1.0/xtest

        totTests += 1
        
        if ytest<func:
            totAccepts += 1
            if i%500==0:
                print("Found Random Number : ",i)

            meas_accrej.append(xtest)
            repeatFlag=0

    #increment the counter
    i = i+1


print ("SUMMARY Acc/Rej : acc=",totAccepts,"  thrown=",totTests,"  eff(acc/tot)=",totAccepts*1.0/totTests)

    
plt.hist(meas_accrej, 100, normed=1, facecolor='g', alpha=0.75)
plt.axis([0, 5, 0, 1])
plt.show()


#inverse transform

totTests   = 0
totAccepts = 0

meas_invtran=[]

i=0
while i<n:

    if i%500==0:
        print (i)

    # in general : norm ==> width*norm + center
    ytest = np.log(3)*random.random()
    # because exp is the inverse of log(x)
    # and log(x) is the CDF of the PDF 1/x
    xtest = np.exp(ytest)          

    totTests += 1
        
    meas_invtran.append(xtest)
    totAccepts += 1
    
    i = i+1
        
print ("SUMMARY INV/TRN : acc=",totAccepts,"  thrown=",totTests,"  eff(acc/tot)=",totAccepts*1.0/totTests)

plt.hist(meas_invtran, 100, normed=1, facecolor='b', alpha=0.75)
plt.axis([0, 5, 0, 1])
plt.show()


# In[ ]:



